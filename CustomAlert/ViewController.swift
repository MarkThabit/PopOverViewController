//
//  ViewController.swift
//  CustomAlert
//
//  Created by Mark Maged on 12/6/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func donePressed(_ sender: UIBarButtonItem)
    {
        let tableVC = self.storyboard?.instantiateViewController(withIdentifier: "TableVC") as! TableVC
        
        tableVC.preferredContentSize = CGSize(width: 300, height: 205)
        tableVC.modalPresentationStyle = .popover
        
        if let popoverPresentationViewController = tableVC.popoverPresentationController
        {
            popoverPresentationViewController.permittedArrowDirections = .any
            popoverPresentationViewController.delegate = self
            popoverPresentationViewController.barButtonItem = self.navigationItem.rightBarButtonItem
            popoverPresentationViewController.sourceView = self.view;
            
            var frame:CGRect = (sender.value(forKey: "view")! as AnyObject).frame
            frame.origin.y = frame.origin.y + 20
            
            popoverPresentationViewController.sourceRect = frame
        }
        
        self.present(tableVC,
                     animated: true,
                     completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return .none
    }
}
