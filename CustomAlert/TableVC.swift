//
//  TableVC.swift
//  CustomAlert
//
//  Created by Mark Maged on 12/6/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import UIKit

class TableVC: UITableViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",
                                                 for: indexPath)

        cell.textLabel?.text = "Test"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.dismiss(animated: true,
                     completion: nil)
    }
}
